FROM ubuntu:18.04
LABEL maintainer="fboechat@yahoo.com"

RUN apt-get update && apt-get install -y \
        systemd \
        apache2
EXPOSE 80
EXPOSE 443

COPY html/ /var/www/html/
RUN mkdir /etc/apache2/ssl
COPY ssl/ /etc/apache2/ssl
COPY 000-default.conf /etc/apache2/sites-enabled/000-default.conf
RUN a2enmod ssl
